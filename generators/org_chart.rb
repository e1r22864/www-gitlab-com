require 'json'
require 'yaml'

# Loads data/team.yml and generates JSON data to plot the organization chart.
class OrgChart
  attr_accessor :filename, :team, :slug_to_entry_map
  attr_accessor :count_for_roles, :reports_to_for_roles

  def initialize(filename = 'data/team.yml')
    @filename = filename
    @team = YAML.load_file(filename)
    @slug_to_entry_map = {}
    @count_for_roles = Hash.new { 0 }
    @reports_to_for_roles = Hash.new { 0 }

    build_validation_data
    build_role_map
  end

  def validate!
    validate_member_data
    validate_roles!
    validate_reporting_structure!
  end

  def team_data
    validate!

    build_json_data
  end

  def team_data_tree
    tree = Hash.new do |h, k|
      h[k] = {
        slug: nil,
        name: nil,
        lead: nil,
        children: []
      }
    end

    team_data.each do |member|
      slug, lead_entry = member.values_at(:slug, :lead)
      lead = lead_entry&.fetch('slug')

      member_tree = tree[slug].merge!(member)

      tree[lead][:children].push(member_tree)
    end

    tree[nil][:children]
  end

  private

  def build_validation_data
    @team.each do |entry|
      role = entry['slug']
      reports_to = entry['reports_to']

      count_for_roles[role] += 1 if role
      reports_to_for_roles[reports_to] += 1 if reports_to
    end
  end

  def validate_roles!
    expected_roles = reports_to_for_roles.keys

    missing = expected_roles - @count_for_roles.keys

    return if missing.empty?

    raise Exception,
          "There are `reports_to` entries for #{missing}, but nobody is assigned to those roles.\n" \
          "Be sure to assign a person `slug:` entry in #{filename}."
  end

  def validate_member_data
    @team.each do |entry|
      if entry['start_date'].nil? || entry['start_date'].is_a?(Date) == false
        raise Exception,
          "In ``/data/team.yml`, the entry\n\n#{entry}\n\ndoes not have the required `start_date` field formatted like `YYYY-MM-DD`."
      end
    end
  end

  def validate_reporting_structure!
    count_for_roles.each do |role, count|
      next if count <= 1

      report_count = reports_to_for_roles[role]

      next unless report_count.positive?

      raise Exception,
            "Ambiguous reporting structure: #{report_count} people reports to the role #{role}, " \
            "but #{count} people have that role.\n" \
            "Check that the right person has `slug: #{role}` in #{@filename}\n" \
            "or that the lines `reports_to: #{role}` are correct for team members."
    end
  end

  def build_role_map
    @team.each do |entry|
      role = entry['slug']
      # Assumes titles are unique among people who have reports
      @slug_to_entry_map[role] = entry
    end
  end

  def build_json_data
    # Build only the data we need
    @team.map do |entry|
      data = {
        # This needs to match the implementation of Gitlab::Homepage::Team::Member#anchor
        anchor: entry['gitlab'] || entry['slug'],
        slug: entry['slug'],
        name: entry['name'],
        link: entry['role'],
        speciality: entry.fetch('speciality', nil),
        placeholder: entry.fetch('placeholder', false)
      }

      reports_to = entry['reports_to']
      data[:lead] = @slug_to_entry_map.fetch(reports_to) if reports_to
      data
    end
  end
end
