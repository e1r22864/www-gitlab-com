---
layout: markdown_page
title: "Category Strategy - Value Stream Management"
---

- TOC
{:toc}

## Manage Stage

| Stage| Maturity |
| --- | --- |
| [Manage](/direction/manage/) | [Minimal](/direction/maturity/) |

### Introduction and how you can help
Thank you for visiting the category strategy page for Value Stream Management and Analytics. This page belongs to [Virjinia Alexieva](https://gitlab.com/valexieva) ([E-Mail](mailto:valexieva@gitlab.com), [Twitter](https://twitter.com/virjinialexieva)).

This strategy is a work in progress and everyone can contribute by sharing their feedback directly on [GitLab.com](https://gitlab.com/groups/gitlab-org/-/epics/1477), via e-mail, or Twitter.

### Overview
<!-- A good description of what your category is today or in the near term. If there are
special considerations for your strategy or how you plan to prioritize, the
description is a great place to include it. Provide enough context that someone unfamiliar
with the details of the category can understand what is being discussed. -->
Over the years, software has become more powerful and specialized, while the teams who create, build, deliver and analyze the performance of products more disconnected, despite the increasing time spent on updates and status reports. In order to scale and speed up the delivery of quality products, silos have to be broken but the business, product managers and engineers still use different tools glued together by imperfect integrations. We strongly believe that having a single application for the entire software development lifecycle is already a huge step forward, but we are only just starting to explore ways to surface valuable insights and recommendations, which will help organizations increase transparency and productivity across teams and connect the business with engineering.

[Value Stream Mapping in Software Development](https://en.wikipedia.org/wiki/Value-stream_mapping) focuses on understanding and measuring the value added by the flow of activities in the software development lifecycle. In the time of technological disruption we are in, success will be largely dependent on the ability of enterprises to define, connect and manage software and business value streams. Often times, this will coincide with a culture shift requiring many enterprises to adapt the way they work. At GitLab, we are making a small step towards connecting the business with engineering by using issues and MRs with [labels](https://gitlab.com/help/user/project/labels.md#scoped-labels) associated with OKRs, for example.

Our first attempt at helping organizations get a better understanding of their flow was the introduction of [Cycle Analytics](https://docs.gitlab.com/ee/user/project/cycle_analytics.html) for all customers that follow the [GitLab Flow](https://about.gitlab.com/solutions/gitlab-flow/). Cycle Analytics got a lot of attention and we quickly understood that while we are striving to define best practices, different organizations have different value streams and workflows and we need to support the ability to define and measure these customized workflows in GitLab in order to move the industry forward and serve our customers better. As of June 2019, Cycle Analytics is officially part of our [VSM solution](https://about.gitlab.com/solutions/value-stream-management/).

### Where we are Headed

<!-- Describe the future state for your category. What problems will you solve?
What will the category look like once you've achieved your strategy? Use narrative
techniques to paint a picture of how the lives of your users will benefit from using this
category once your strategy is fully realized -->

We are building GitLab with the goal of having teams use us for their whole development life cycle and we believe that an integrated solution will enable teams to be faster and more efficient. However, you cannot measure what you cannot see, so we want to make it easy for Engineering Leadership to quickly get a unified view of the end-to-end flow of how software value is created by their teams. We believe we can capture the vast majority of the processes metadata within GitLab since we already have tools for ideation, planning, design, development, testing, code review, security testing and release.

##### Value Stream Mapping

In order to ensure consistent visibility across the value stream at different levels of granularity, we want to facilitate consistent language and definitions across teams. Using GitLab events, such as `issue_creation`, `merge_request_last_build_finished`, `merge_request_closed`, `test_job_finished`, we can enable teams to define and split their value stream stages as they see fit. While we have only a few events available now, we will continue to expand that list. We will enable users to define, save and share the process reports they create together with any relevant charts. This will ensure that our customers use a single source of data to display both detailed information for daily organization and aggregated statistics for management roll-ups.

##### Identifying and Drill Down into Source of Waste

 By providing customers with the ability to define their processes at different levels of granularility and to calculate the time it takes to complete each section, users should be able to quickly spot where their relative bottlenecks are. However, in order to get to the bottom of a problem, we need to allow users to drill-down. This is why we will build a catalog of configurable chart widgets, which users could quickly add to their dashboard to gain quick insights of possible causes of slow-moving stages in their process.

 Let's take code review as an example bottleneck - while a certain level of code review is necessary, extensive long cycles of wait, could be a huge source of waste preventing valuable features getting to production. We are planning to provide histograms showing the time taken to incorporate merge requests, size and complexity thereof as well as other graphs displaying pipeline times and particular indivuals involved. By allowing filtering by date, authors, assignes, milestones or weights an engineering manager should be able to find patterns of suboptimal operations, which they should address. In order to measure the value of said review process, one could compare the quality/performance issues and rework coming from that particular place in the codebase in the future. However, a better alternative might be an automated review process. In general, through value stream mapping, we hope to be able to show you the steps where you can improve your practices by segregating the value added activities from those that contribute to waste. We would like to get to a stage where we compel customers to ask the important questions of how much value each activity adds compared to the time and resources it takes. By visualizing hand-offs, which we could imply from our data, queues that damage teams' productivity should become obvious.

 Some of the questions we hope our charts can initially answer are:

- How much of my team's work is going towards new business value and features vs. technical debt, quality or security issues?
- Are engineers working on a particular part of the codebase having to do a lot more refactoring, which slows feature flow down?
- How much time do we take to deliver a feature once it's ready for development, i.e. what is our cycle time? What is our overall lead time?
- How do my engineers compare and how do they compare to other teams?
- What is slowing down the delivery of features for my team and how can I improve this? Where are the bottlenecks? Are we optimizing the right part of the process?
- How do we provide realistic estimates to business stakeholders?
- How can I address potential delays and inconsistent quality ahead of time?
- How is GitLab helping me become more efficient?

##### Recommendations

Through our product, we have worked with a vast host of engineering teams, which has helped us identify common patterns of success and failure in DevOps. We are planning to encode this knowledge and given the data we enable users to store in their instance, we would like to automatically highlight process bottlenecks and generate recommendations on how to minimize their impact. We would also like to enable customers to set targets for the different stages of their process, which we can follow up on with reminders and suggestions. A couple of examples, below:

- If you have defects relating to a particular part of the code base, we should be able to highlight that additional tests might be necessary. Showing you code hotspots in your repo, might help with the prioritizing of such tests and technical debt as well. Indeed, there are important drivers to efficiency and quality that don't necessarily revolve around people and processes, such as the complexity of our codebase, the state of our code hotspots and test coverage. While we strive to provide recommendations and checks around [code quality](https://docs.gitlab.com/ee/user/project/merge_requests/code_quality.html) on each merge request, we believe there is a lot more that can be done on the repository as whole, which we will cover under [Code Analytics](https://about.gitlab.com/handbook/direction/manage/code-analytics).
- If security reviews are an enormous bottleneck and only come once a feature is ready for design, you might want to shift left and bring security in the design process as well as use more automated scanning.

Recommendations will be ranked in order of importance by the value they bring in reducing time to production and increasing quality. Through your feedback on the quality of those recommendations and industry developments, we will continue to improve our algorithm to provide more tailored suggestions.

##### Faster Time to Production and Increased Quality

It's important that data and performance is evaluated with a focus on outcomes. Whether we succeed in enabling customers to visualize the bottlenecks in their process can be objectively measured by faster time to production. According to the 2018 DORA report, Elite DevOps performers take less than an hour to get from code committed to production, while low performers - anywhere between one and six months. We will show you trends of how well you are doing over time plotted against industry benchmarks or your own organizational targets. Being fast, however, should not come at the cost of wasteful coding practices creating a lot of quality and instability issues. High and elite performers in DevOps show high correlation between faster time to production and rare, quickly identified and resolved issues. We will provide KPIs coming from our monitoring stage such as MTTD, MTTR, MTTR to provide the full picture of the health of your value stream.

### Target Audience and Experience
<!-- An overview of the personas (https://about.gitlab.com/handbook/marketing/product-marketing/roles-personas#user-personas) involved in this category. An overview
of the evolving use cases and user journeys as the category progresses through minimal,
viable, complete and lovable maturity levels. -->
During the minimal and viable states of the category, we aim to cater to the needs of Development Teams Leadership (primarily) and Product Managers (secondarily) in their efforts to improve velocity and predictability of their value streams. As we build out the category, we hope that Department Leads and Senior Executives will have one place where they could oversee the progress of their teams and identify best practices and negaitve pattens that inspire improvements across their organizations.

### What's Next & Why

<!-- This is almost always sourced from the following sections, which describe top
priorities for a few stakeholders. This section must provide a link to an issue
or [epic](https://about.gitlab.com/handbook/product/#epics-for-a-single-iteration) for the MVC or first/next iteration in the category.-->

#### The 1 year plan

Since many organizations are just starting to explore analytics in order to increase efficiency and predictability in the software development cycle, we expect the category to change quickly in response to feedback.

Nevertheless, over the next year, there are a couple of big areas we would like to tackle that should enable us to deliver on our 3 year strategy:

- Create a one stop [analytics hub](https://gitlab.com/gitlab-org/gitlab-ee/issues/12077), where customers are able to aggregate and visualize information across individuals, projects, groups and subgroups

- Allow customers to map their processes by using GitLab events at different levels of granularity (from an individual to cross-group):
    * Surface all GitLab events that are currently stored and break them down even further
    * Enable the definition, saving, sharing and loading of report templates on a user level
    * Enable customers to link the different stages of the process according to their current way of working

- Allow customers to add customizable widgets in order to drill-down and visualize their streams. A couple of examples below:
    * Chart showing the cycle time of different stages in a specified tiemframe, with markers for each object - valuable in order to monitor trends over time
    * Chart showing the flow of work (ex: quality, security, tech debt, feature) over time - valuable so that teams can optimize the proportion of each type of work going through the pipeline

- Allow customers to filter each dashboard down to specific individuals, labels, milestone, dates

- Allow customers to own multiple dashboards, which they can save and share

- Allow customers to export more of their data in .csv format, through logs using our integration with ELK or through our API

- Improve our REST API, so that users can easily create their own widgets and expose to the community if appropriate

#### The details

**Q2 2019 (May, Jun, Jul)**
12.0

- [Fixed bugs in default stages definitions in cycle analytics and decided on starting the work on providing the ability for users to customize according to their flows and visualize trends](https://gitlab.com/gitlab-org/gitlab-ce/issues/30138)

12.2

- [Analytics hub](https://gitlab.com/gitlab-org/gitlab-ee/issues/12077)

- [Default stages in cycle analytics at group level](https://gitlab.com/gitlab-org/gitlab-ee/issues/12181)

**Q3 2019 (Aug, Sep, Oct)**

12.3

- [Customizable stages in cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12196)
- [Show all events on Cycle Analytics Tables, enable sorting](https://gitlab.com/gitlab-org/gitlab-ee/issues/12524)
- [Add a date picker for Cycle Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/13216)
- [Enable additional filtering for Cycle Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12525)
- [Trends of each stage in analytics over time](https://gitlab.com/gitlab-org/gitlab-ee/issues/12755)
- [Visualize type and flow of work for cycle analytics] (https://gitlab.com/gitlab-org/gitlab-ee/issues/12260)
- [Productivity Analytics with Trends and Date Picker](https://gitlab.com/gitlab-org/gitlab-ee/issues/12079)
- [New Code, Churn, Refactoring](https://gitlab.com/gitlab-org/gitlab-ee/issues/12246)

12.4 (TBC)

- [Add additional event on cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/13302)
- [Improve approach to linking events to 1 stream in cycle analytics](https://gitlab.com/gitlab-org/gitlab-ce/issues/45298)
- [Add weights to type and flow of work](https://gitlab.com/gitlab-org/gitlab-ee/issues/13498)
- [Enable excel export on cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/13497)
- [And/ OR event in cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12883)
- [Save a template in cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12884)
- [Visualize relationship between authors and assignees on MRs in productivity analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/13267)

12.5 (TBC, under UX and product discovery)

[Enable comparisons between teams in cycle analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12598)
[Generalize histograms in Productivity Analytics on Events](https://gitlab.com/gitlab-org/gitlab-ee/issues/13442)
[Add additional events in Productivity Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/13300)

**Q4 2019 (Nov, Dec, Jan)**

**Q1 2020 (Feb, Mar, Apr)**

**Q2 2020 (May, Jun, Jul)**


<!--
### What is Not Planned Right Now
Often it's just as important to talk about what you're not doing as it is to
discuss what you are. This section should include items that people might hope or think
we are working on as part of the category, but aren't, and it should help them understand why that's the case.
Also, thinking through these items can often help you catch something that you should
in fact do. We should limit this to a few items that are at a high enough level so
someone with not a lot of detailed information about the product can understand
the reasoning-->

### Maturity Plan
This category is currently at the `Minimal` maturity level and our next maturity target is `Viable` by October 2019. Please see our [definitions of maturity levels](https://about.gitlab.com/handbook/product/categories/maturity/#legend) and related [epics](https://gitlab.com/groups/gitlab-org/-/epics?scope=all&utf8=✓&state=opened&label_name[]=Category%3AValue%20Stream%20Management&label_name[]=group%3A%3Aanalytics&label_name[]=devops%3A%3Amanage).

<!-- It's important your users know where you're headed next. The maturity plan
section captures this by showing what's required to achieve the next level. The
section should follow this format:

- Link to maturity epic if you are using one, otherwise list issues with maturity::YYYY labels) -->

### Competitive Landscape

#### Tasktop
[TaskTop](https://www.tasktop.com/integration-hub) is exclusively focused on Value Stream Management and allows users to connect more than 50 tools together, including Atlassian's JIRA, GitLab, GitHub, JamaSoftware, CollabNet VersionOne, Xebia Labs, and TargetProcess to name a few. Tasktop serves as an integration layer on top of all the software development tools that a team uses and allows for mapping of processes and people in order to achieve a common data model across the toolchain. End users can visualize the flows between the different tools and the data can be exported to a database for visualization through BI tools.

While we understand that not all users of GitLab utilize all of our stages, we believe that there is already a lot of information across planing, source code management and continuous integration and deployment, which can be used to deliver valuable insights.

We are starting to build dashboards, which can help end users visualize a custom-defined value stream flow at a high level and drill down and filter to a specific line of code or MR.

#### CollabNet VersionOne
[CollabNet VersionOne](https://www.collab.net) provides users with the ability to input a lot of information, which is a double edged sword as it can lead to duplication of effort and stale information when feeds are not automated. It does however, allow a company to visualize project streams from a top level with all their dependencies. End users can also create customizable reports and dashboards that can be shared with senior management.

#### Plutora
[Plutora Analytics](https://www.plutora.com/platform/plutora-analytics) seem to target mainly the release managers with their [Time to Value Dashboard](https://www.plutora.com/platform/time-to-value-dashboard). The company also integrates with JIRA, Jenkins, GitLab, CollabNet VersionOne, etc but there is still a lot of configuration that seems to be left to the user.

#### TargetProcess
[Targetprocess](https://www.targetprocess.com) tries to provide full overview over the delivery process and integrates with Jenkins, GitHub and JIRA. The company also provides customizable dashboards that can give an overview over the process from ideation to delivery.

#### GitPrime
Although [GitPrime](https://www.gitprime.com) doesn't try to provide a value stream management solution, it focuses on productivity metrics and cycle time by looking at the productivity of a team. It exclusively uses only git data.

#### Azure DevOps
Naturally, [Azure](https://docs.microsoft.com/en-us/azure/devops/report/dashboards/analytics-widgets?view=azure-devops) is working on adding analytics that can help engineering teams become more effective but it's still in very early stages. It has also recently acquired [PullPanda](https://pullpanda.com).

#### Velocity by Code Climate
Similarly to GitPrime, [Code Climate](https://codeclimate.com/velocity/understand-diagnose/) focuses on the team and uses git data only.

#### Gitalytics
Similarly to GitPrime, [Gitalytics](https://gitalytics.com) focuses on the team and uses git data only.

#### Gitential
[Gitential](https://gitential.com)

#### XebiaLabs
[XebiaLabs' analytics](https://docs.xebialabs.com/xl-release/concept/release-dashboard-tiles.html) are predominantly focused on the Release Manager and give useful overviews of deployments, issue throughput and stages. The company integrates with JIRA, Jenkins, etc and end users can see in which stage of the release process they are.

#### CloudBees
[CloudBees DevOptics](https://www.cloudbees.com/products/cloudbees-devoptics) is focused on giving visibility and insights to measure, manage and optimize the software delivery value stream. It allows comparisons across teams and integrates with Jenkins and Jira and SVM /VCS solutions.

#### CA Technologies
[CA Agile Central](https://docs.ca.com/en-us/ca-agile-central/saas/iteration-burndown) combines data across the planning process in a single integrated page with custom applications available to CA Agile Central users. The applications can be installed in custom pages within CA Agile Central or on a dashboard.

#### Atlassian's JIRA Align
[Agile Craft](https://agilecraft.com)

#### PivotalTracker
[PivotalTracker](https://www.pivotaltracker.com/help/articles/analytics_charts_and_reports_overview/)

### Kanbanize
[Kanbanize](https://kanbanize.com/dashboards-analytics/)

### Analyst Landscape
<!-- What analysts and/or thought leaders in the space talking about, what are one or two issues
that will help us stay relevant from their perspective.-->

Forrester's New Wave: Value Stream Management Tools, Q3 2018 uncovered an emerging market with no leaders. However, vendors from different niches of the development pipeline are converging to value stream management in response to customers seeking greater transparency into their processes.

Forrester’s vision for VSM includes:
- end-to-end visibility of the software development process, including the corresponding capture and storage of data, events, and artifacts
- definition and visualization of key performance indicators (KPIs)
- inclusive customer experience, which allows multiple roles (PMs, developers, QA, and release managers) to collaborate in one place
- governance, i.e. a framework to monitor compliance to organizational standards, automated audit capabilities and traceability.

Additional functionality, requested by clients includes:
- integration with other tools, including the ability to double-click into each tool to directly observe status or take action
- business value custom definitions in terms of financials, time, effort or similar
- mapping of business value, people, processes, data
- visualization dashboards, which users can customize to support different role-based views.

<!--
### Top Customer Success/Sales issue(s)
These can be sourced from the CS/Sales top issue labels when available, internal
surveys, or from your conversations with them.-->

<!--
### Top user issue(s)
This is probably the top popular issue from the category (i.e. the one with the most
thumbs-up), but you may have a different item coming out of customer calls.-->

### Top internal customer issue(s)
<!-- These are sourced from internal customers wanting to [dogfood](https://about.gitlab.com/handbook/product/#dogfood-everything)
the product.-->
We are just getting started, but senior management from the development organization has expressed interest in what we are building and we are looking for their feedback. Every single issue in the space we create should be something that the Engineering Executives find helpful. An initial list of issues includes:

- [Cycle Time at a Group Level](https://gitlab.com/gitlab-org/gitlab-ee/issues/12078)
- [Productivity Analytics](https://gitlab.com/gitlab-org/gitlab-ee/issues/12079)
- [Code Analytics](https://gitlab.com/gitlab-org/gitlab-ce/issues/62372)

Relevant links: [Development Metrics Working Group](https://about.gitlab.com/company/team/structure/working-groups/development-metrics/), [Development Metrics Agenda](https://docs.google.com/document/d/1Y50uhpRW0zSGWI-TzPxHnwEHyOl7uWiyCzXtpRJd1_E/edit#heading=h.9yupw78rdovb)


<!--
### Top Strategy Item(s)
What's the most important thing to move your strategy forward?-->
